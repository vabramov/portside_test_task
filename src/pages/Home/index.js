import React from 'react';
import { connect } from 'react-redux';

export const HomePage = (props) => {
	return (
		<div className="wrapper">
			<div>Home page</div>
		</div>

	);
};

function mapStateToProps(state) {
	return {
	};
}

function mapDispatchToProps(dispatch) {
	return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
