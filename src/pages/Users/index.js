import React, { useEffect, useReducer } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchUsers } from '../../store/actions/userActions';

const ACTIONS = {
	USERS_DATA_LOADED: 'USER_DATA_LOADED',
	USERS_DATA_ERROR: 'USERS_DATA_ERROR'
};
const UsersList = ({data}) => {
	const listItems = data.map((user) => <UserLink data={user} key={user.id}/>);
	return (
		<ul className="usersList">
			{listItems}
		</ul>
	);
};

const UserLink = ({data}) => {
	const {email, id} = data;
	return (
		<li>
			<Link to={`users/${id}`}>{email}</Link>
		</li>
	);
};

const onUsersLoaded = (state, action) => {
	return {
		...state,
		users: action.users
	};
};

const onUsersLoadError = (state, action) => {
	return {
		...state,
		error: action.error
	};
};

const dataReducer = (state, action) => {
	switch (action.type) {
		case ACTIONS.USERS_DATA_LOADED:
			return onUsersLoaded(state, action);
		case ACTIONS.USERS_DATA_ERROR:
			return onUsersLoadError(state, action);
		default:
			return state;
	}
};

const UsersPage = (props) => {
	const initialState = { users: [], error: '' };
	const [state, dispatch] = useReducer(dataReducer, initialState);
	useEffect(() => {
		props.actions.fetchUsers();
		if (props.users && props.users.length !== state.users.length) {
			dispatch({type: ACTIONS.USERS_DATA_LOADED, users: props.users});
		}
		if (props.error && props.error !== state.error) {
			dispatch({type: ACTIONS.USERS_DATA_ERROR, error: props.error});
		}
	}, [props.users.length]);
	return (
		<div className="user-page">
			{
				state.error
					? state.error
					: state.users
						? <UsersList data={state.users}/>
						: 'There is no data to display'
			}
		</div>
	);
};

function mapStateToProps(state) {
	return {
		users: state.users.list
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: {
			fetchUsers: () => {
				dispatch(fetchUsers());
			}
		}
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
