import React, { useEffect, useReducer } from 'react';
import { connect } from 'react-redux';

import { fetchUserData } from '../../store/actions/userActions';

const ACTIONS = {
	USER_DATA_LOADED: 'USER_DATA_LOADED',
	USER_DATA_ERROR: 'USER_DATA_ERROR'
};

const onDataLoader = (state, action) => {
	return {
		...state,
		data: action.data
	};
};

const onLoadError = (state, action) => {
	return {
		...state,
		error: action.error
	};
};

const dataReducer = (state, action) => {
	switch (action.type) {
		case ACTIONS.USER_DATA_LOADED:
			return onDataLoader(state, action);
		case ACTIONS.USER_DATA_ERROR:
			return onLoadError(state, action);
		default:
			return state;
	}
};

const DataFieldset = ({data}) => {
	const fields = Object.keys(data).map((key) => {
		const info = data[key];
		return typeof info === 'object'
			? <DataFieldset key={key} data={info}/>
			: <DataField key={key} data={info} name={key}/>;
	});

	return (
		<div className="data-field-wrapper">
			{fields}
		</div>
	);
};

const DataField = ({data, name}) => {
	return (
		<span className="data-field">{`${name.toUpperCase()}: ${data}`}<br/></span>
	);
};

const UserInfo = ({data}) => {
	return (
		<div className="user-info-wrapper">
			<DataFieldset data={data}/ >
		</div>
	);
};

const UserPage = (props) => {
	const { id } = props.match.params;
	const initialState = { data: null, error: '' };
	const [state, dispatch] = useReducer(dataReducer, initialState);
	useEffect(() => {
		if (id) {
			props.actions.fetchUserData(id);
		}

		if (props.data !== !state.data) {
			dispatch({type: ACTIONS.USER_DATA_LOADED, data: props.data })
		}
		if (props.error !== !state.error) {
			dispatch({type: ACTIONS.USER_DATA_ERROR, error: props.error })
		}
	}, [props.data !== null]);
	return (
		<div className="user-page">
			{
				state.error
					? state.error
					: state.data
						? <UserInfo data={state.data}/>
						: 'There is no data to display'
			}
		</div>
	);
};

function mapStateToProps(state) {
	return {
		data: state.user.data,
		error: state.user.error
	};
};

function mapDispatchToProps(dispatch) {
	return {
		actions: {
			fetchUserData: (id) => {
				dispatch(fetchUserData(id));
			}
		}
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
