import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import createRootReducer from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

const store = createStore(
  createRootReducer(),
  {},
  applyMiddleware(...middlewares)
);

sagaMiddleware.run(rootSaga);

export { store };
