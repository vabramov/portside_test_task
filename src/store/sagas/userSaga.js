import { takeLatest, put, call } from 'redux-saga/effects';
import { USER_DATA_REQUESTED, USER_DATA_SUCCESS, USER_DATA_ERROR } from '../constants/actions';
import { API_URL } from '../constants/urls';

function* fetchUserData(action) {
	try {
		const { id } = action;
		const response = yield call(fetch, `${API_URL}/${id}`);
		const data = yield call([response, 'json']);
		yield put({type: USER_DATA_SUCCESS, data});
	} catch(error) {
		yield put({type: USER_DATA_ERROR, error});
	}
};

function* rootSaga() {
	yield takeLatest(USER_DATA_REQUESTED, fetchUserData);
};

export default rootSaga;
