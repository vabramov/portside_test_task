import { all } from 'redux-saga/effects';
import fetchUsers from './usersSaga';
import fetchUserData from './userSaga';


export default function* rootSaga() {
	yield all([
		fetchUsers(),
		fetchUserData()
	]);
};
