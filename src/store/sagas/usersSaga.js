import { takeEvery, put, call } from 'redux-saga/effects';
import {USERS_DATA_REQUESTED, USERS_DATA_SUCCESS, USERS_DATA_ERROR } from '../constants/actions';
import { API_URL } from '../constants/urls';

function* fetchUsers() {
	try {
		const request = yield call(fetch, API_URL);
		const data = yield call([request, 'json']);
		yield put({type: USERS_DATA_SUCCESS, data});
	} catch(error) {
		yield put({type: USERS_DATA_ERROR, error});
	}
};

function* rootSaga() {
	yield takeEvery(USERS_DATA_REQUESTED, fetchUsers);
};

export default rootSaga;
