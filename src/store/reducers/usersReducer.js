import {USERS_DATA_SUCCESS, USERS_DATA_ERROR} from '../constants/actions';

const initialState = {
	list: [],
	error: ''
};

const usersReducer = (state = initialState, action) => {
	switch (action.type) {
		case USERS_DATA_SUCCESS:
			return {
				...state,
				list: [...action.data]
			};
		case USERS_DATA_ERROR:
			return {
				...state,
				error: action.error
			}
		default:
			return state;
	}
}

export default usersReducer;
