import { USER_DATA_SUCCESS, USER_DATA_ERROR } from '../constants/actions';

const initialState = {
	data: null,
	error: ''
};

const userReducer = (state = initialState, action) => {
	switch (action.type) {
		case USER_DATA_SUCCESS:
			return {
				...state,
				data: action.data
			};
		case USER_DATA_ERROR:
			return {
				...state,
				error: action.error
			};
		default:
			return state;
	}
};

export default userReducer;
