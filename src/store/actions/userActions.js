import { USERS_DATA_REQUESTED, USER_DATA_REQUESTED } from '../constants/actions';

export function fetchUsers() {
	return {type: USERS_DATA_REQUESTED};
};


export function fetchUserData(id) {
	return {type: USER_DATA_REQUESTED, id};
};
