import React from 'react';

import { Switch, Route } from "react-router";

import HomePage from '../pages/Home';
import UsersPage from '../pages/Users';
import UserPage from '../pages/User';

export default () => {
	return (
		<Switch>
			<Route exact path="/" component={HomePage}/>
			<Route path="/users">
				<Switch>
					<Route exact path="/users" component={UsersPage}/>
					<Route path="/users/:id" component={UserPage}/>
				</Switch>
			</Route>
		</Switch>
	);
}
